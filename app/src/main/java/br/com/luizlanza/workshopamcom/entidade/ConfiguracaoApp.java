package br.com.luizlanza.workshopamcom.entidade;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by luiz.lanza on 25/06/2018.
 */

@IgnoreExtraProperties
public class ConfiguracaoApp {

    public String nome;
    public Double versao;

    ConfiguracaoApp(){

    }

    ConfiguracaoApp(String nome, Double versao){
        this.nome = nome;
        this.versao = versao;
    }
}
